const { getOrder } = require('@services/order');
const { OK } = require('@utils/helper');

/**
 * detail
 * @public
 */
exports.detail = async (req, res, next) => {
  try {
    const order = await getOrder(req.params.orderId, req.headers.usertype, req.headers.userid);
    return OK(res, 'Order detail', order);
  } catch (error) {
    return next(error);
  }
};
