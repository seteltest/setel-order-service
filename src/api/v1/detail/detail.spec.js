/* eslint-disable arrow-body-style */
jest.mock('@services/order');
const { getOrder } = require('@services/order');
const MockReq = require('mock-express-request');
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const { APIError } = require('@utils/APIError');
const controller = require('./detail.controller');

describe('Test detail', () => {
  let req;
  let res;

  beforeEach(() => {
    req = new MockReq({
      params: { orderId: 123 },
      headers: { userid: 1 }
    });
    res = new MockRes();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('should failed to return order detail', () => {
    getOrder.mockRejectedValue(APIError.withCode('UNKNOWN'));
    return controller.detail(req, res, (apiError) => {
      expect(apiError).toHaveProperty('name');
      expect(apiError).toHaveProperty('errors');
      expect(apiError.errors[0].errorCode).toBe('UNKNOWN');
    });
  });

  it('should return order detail', async () => {
    getOrder.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.detail(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });
});
