const { listOrder } = require('@services/order');
const { OK, constructPagination } = require('@utils/helper');

/**
 * list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const { pageNumber, pageSize } = req.query;
    const { count, rows } = await listOrder(pageNumber - 1, pageSize, req.headers.usertype, req.headers.userid);
    return OK(res, 'Order Listing', constructPagination(rows, count, pageNumber, pageSize));
  } catch (err) {
    return next(err);
  }
};
