/* eslint-disable arrow-body-style */
jest.mock('@services/order');
const { listOrder } = require('@services/order');
const MockReq = require('mock-express-request');
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const { APIError } = require('@utils/APIError');
const controller = require('./list.controller');

describe('Test list', () => {
  let req;
  let res;

  beforeEach(() => {
    req = new MockReq({
      query: { pageNumber: 1 }
    });
    res = new MockRes();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('should failed to return order list', () => {
    listOrder.mockRejectedValue(APIError.withCode('UNKNOWN'));
    return controller.list(req, res, (apiError) => {
      expect(apiError).toHaveProperty('name');
      expect(apiError).toHaveProperty('errors');
      expect(apiError.errors[0].errorCode).toBe('UNKNOWN');
    });
  });

  it('should return order list', async () => {
    listOrder.mockResolvedValue({ count: 0, rows: [] });
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.list(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: {
          items: expect.any(Array),
          pagination: expect.any(Object)
        }
      }));
    });
  });
});
