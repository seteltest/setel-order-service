const Joi = require('joi');

module.exports = {
  name: 'List of orders',
  path: '/api/v1',
  type: 'get',
  joiSchema: {
    query: Joi.object({
      pageNumber: Joi.number().integer().default(1).min(1),
      pageSize: Joi.number().integer().min(10).default(20)
    }),
    response: {
      200: {
        description: 'OK',
        body: {
          responseCode: 200,
          responseMessage: Joi.string().required(),
          response: {}
        }
      },
      400: {
        description: 'Error Response',
        body: {
          responseCode: 400,
          responseMessage: Joi.string().required(),
          response: {
            errors: Joi.array().items(Joi.object().keys({
              errorCode: Joi.string().required(),
              errorTitle: Joi.string().required(),
              errorDescription: Joi.string().required(),
              errorDebugDescription: Joi.string()
            }))
          }
        }
      }
    }
  }
};
