const _ = require('lodash');
const { getProductPrices } = require('@services/product');
const { getCouponValue } = require('@services/coupon');
const { createOrder } = require('@services/order');
const { logger } = require('@utils/logger');
const { OK, formulateOrder, getOrderUser } = require('@utils/helper');
const { paymentCreationQueue } = require('@queue');

/**
 * Function to generate final order including price
 * @param {Object} body
 */
const generateOrder = async (body) => {
  const skus = body.orderItems.map(product => product.sku);
  const promises = [];
  promises.push(getProductPrices(skus));
  if (!_.isNil(body.coupon)) {
    promises.push(getCouponValue(body.coupon));
  }
  const result = await Promise.all(promises);
  const order = formulateOrder(body, result);
  return order;
};

/**
 * Function to automatically trigger payment initiation
 * @param {Object} order
 */
const initiatePayment = (order) => {
  paymentCreationQueue.createJob(order)
    .retries(2)
    .save()
    .catch((err) => {
      logger.info('Job Payment Initiation', err);
    });
};

/**
 * create
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const body = _.pick(req.body, ['orderItems', 'currency', 'coupon']);
    body.userId = getOrderUser(req.headers, req.body); // if user type is admin, then admin can create order on any user behalf
    const order = await generateOrder(body);
    const response = await createOrder(order);
    initiatePayment(response);
    return OK(res, 'Order created successfully', response);
  } catch (error) {
    return next(error);
  }
};
