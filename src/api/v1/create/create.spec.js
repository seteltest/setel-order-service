/* eslint-disable arrow-body-style */
const MockReq = require('mock-express-request');
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const { APIError } = require('@utils/APIError');

jest.mock('@services/order');
const { createOrder } = require('@services/order');

const controller = require('./create.controller');

describe('Test create', () => {
  let req;
  let res;

  beforeEach(() => {
    req = new MockReq({
      params: { orderId: 123 },
      headers: { userid: 1 },
      body: {
        currency: 'INR',
        orderItems: [{
          sku: '1',
          quantity: 1
        }],
        coupon: 'FREETRIAL'
      }
    });
    res = new MockRes();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('should failed to create order', () => {
    createOrder.mockRejectedValue(APIError.withCode('UNKNOWN'));
    return controller.create(req, res, (apiError) => {
      expect(apiError).toHaveProperty('name');
      expect(apiError).toHaveProperty('errors');
      expect(apiError.errors[0].errorCode).toBe('UNKNOWN');
    });
  });

  it('should create new order', async () => {
    createOrder.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.create(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });

  it('should create new order without coupon', async () => {
    req = new MockReq({
      params: { orderId: 123 },
      headers: { userid: 1, usertype: 'admin' },
      body: {
        currency: 'INR',
        orderItems: [{
          sku: '1',
          quantity: 1
        }],
        userId: '123'
      }
    });
    createOrder.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.create(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });
});
