/* eslint-disable arrow-body-style */
jest.mock('@services/order');
const { cancelOrder } = require('@services/order');
const MockReq = require('mock-express-request');
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const { APIError } = require('@utils/APIError');
const controller = require('./cancel-order.controller');

describe('Test cancel order', () => {
  let req;
  let res;

  beforeEach(() => {
    req = new MockReq({
      params: { orderId: 123 },
      headers: { userid: 1 }
    });
    res = new MockRes();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('should failed to cancel order', () => {
    cancelOrder.mockRejectedValue(APIError.withCode('UNKNOWN'));
    return controller.cancelOrder(req, res, (apiError) => {
      expect(apiError).toHaveProperty('name');
      expect(apiError).toHaveProperty('errors');
      expect(apiError.errors[0].errorCode).toBe('UNKNOWN');
    });
  });

  it('should pass to cancel order', async () => {
    cancelOrder.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.cancelOrder(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });
});
