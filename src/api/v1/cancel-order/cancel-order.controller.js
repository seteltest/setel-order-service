const { cancelOrder } = require('@services/order');
const { OK } = require('@utils/helper');

/**
 * cancelOrder
 * @public
 */
exports.cancelOrder = async (req, res, next) => {
  try {
    const order = await cancelOrder(req.params.orderId, req.headers.userid);
    // TODO: here we can send a message to queue service to start rembursement procedure, if payment is already made
    return OK(res, 'Order cancelled successfully', order);
  } catch (error) {
    return next(error);
  }
};
