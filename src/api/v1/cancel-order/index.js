const express = require('express');
const validate = require('express-validation');
const controller = require('./cancel-order.controller');
const validator = require('./cancel-order.validator');

const router = express.Router({ mergeParams: true });

/**
 * @api {put} api/v1/cancelOrder cancelOrder
 * @apiDescription Cancel Order
 * @apiVersion 1.0.0
 * @apiName cancelOrder
 * @apiPermission public
 *
 * @apiParam  {String} [param]  Put some parameter schema here
 *
 * @apiSuccess {Number} responseCode     HTTP Response Code
 * @apiSuccess {String} responseMessage  Response message
 * @apiSuccess {Object} response         Response object
 *
 * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
 */
router.route('/')
  .delete(validate(validator.joiSchema), controller.cancelOrder);

module.exports = router;
