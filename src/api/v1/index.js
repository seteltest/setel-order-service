const express = require('express');

const router = express.Router();
const createRoute = require('./create');
const updateRoute = require('./update');
const listRoute = require('./list');
const detailRoute = require('./detail');
const cancelOrderRoute = require('./cancel-order');


router.use('/', createRoute);
router.use('/', listRoute);
router.use('/:orderId', updateRoute);
router.use('/:orderId', detailRoute);
router.use('/:orderId', cancelOrderRoute);


module.exports = router;
