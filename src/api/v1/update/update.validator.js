const Joi = require('joi');
const { ORDER_STATUS } = require('../../../config/vars');

module.exports = {
  name: 'Update Order',
  path: '/api/v1/:orderId',
  type: 'put',
  joiSchema: {
    headers: Joi.object({
      userid: Joi.string().required(),
      usertype: Joi.string().valid(['admin']).required()
    }),
    params: Joi.object({
      orderId: Joi.string().guid().required()
    }),
    body: Joi.object({
      status: Joi.string().valid(Object.keys(ORDER_STATUS)).required()
    }),
    response: {
      200: {
        description: 'OK',
        body: {
          responseCode: 200,
          responseMessage: Joi.string().required(),
          response: {}
        }
      },
      400: {
        description: 'Error Response',
        body: {
          responseCode: 400,
          responseMessage: Joi.string().required(),
          response: {
            errors: Joi.array().items(Joi.object().keys({
              errorCode: Joi.string().required(),
              errorTitle: Joi.string().required(),
              errorDescription: Joi.string().required(),
              errorDebugDescription: Joi.string()
            }))
          }
        }
      }
    }
  }
};
