const { updateOrderStatus } = require('@services/order');
const moment = require('moment');
const { OK } = require('@utils/helper');
const { logger } = require('@utils/logger');
const { autoOrderDeliveryTime } = require('@config/vars');
const { orderDeliveryQueue } = require('../../../queue');

/**
 * Function to automatically schedule order to delivery state
 * @param {String} orderId
 */
const scheduleOrderForDelivery = (orderId) => {
  orderDeliveryQueue.createJob({ orderId })
    .retries(2)
    .delayUntil(moment().add(autoOrderDeliveryTime, 'm').valueOf())
    .save()
    .catch((err) => {
      logger.info('Job Delivery Failed', err);
    });
};

/**
 * update
 * @public
 */
exports.update = async (req, res, next) => {
  try {
    const { status } = req.body;
    const order = await updateOrderStatus(req.params.orderId, req.body.status);
    if (status === 'CONFIRMED') {
      scheduleOrderForDelivery(req.params.orderId);
    }
    return OK(res, 'Order status updated', order);
  } catch (error) {
    return next(error);
  }
};
