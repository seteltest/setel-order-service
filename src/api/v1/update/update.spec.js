/* eslint-disable arrow-body-style */
jest.mock('@services/order');
const { updateOrderStatus } = require('@services/order');
const MockReq = require('mock-express-request');
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const { APIError } = require('@utils/APIError');
const controller = require('./update.controller');

describe('Test update', () => {
  let req;
  let res;

  beforeEach(() => {
    req = new MockReq({
      params: { orderId: 123 },
      body: { status: 'CANCELLED' }
    });
    res = new MockRes();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('should failed to update order status', () => {
    updateOrderStatus.mockRejectedValue(APIError.withCode('UNKNOWN'));
    return controller.update(req, res, (apiError) => {
      expect(apiError).toHaveProperty('name');
      expect(apiError).toHaveProperty('errors');
      expect(apiError.errors[0].errorCode).toBe('UNKNOWN');
    });
  });

  it('should able to update the order status to cancel', async () => {
    updateOrderStatus.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.update(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });

  it('should able to update the order status to confirmed', async () => {
    req = new MockReq({
      params: { orderId: 123 },
      body: { status: 'CONFIRMED' }
    });
    updateOrderStatus.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.update(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });
});
