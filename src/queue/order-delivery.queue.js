/**
 * Order Delivery Job
 *
 */

const { updateOrderStatus } = require('@services/order');
const { logger } = require('@utils/logger');

const orderDeliveryProcess = async (job) => {
  logger.info(`Job ${job.id} start processing`);
  await updateOrderStatus(job.data.orderId, 'DELIVERED');
  return true;
};

module.exports = {
  orderDeliveryProcess
};
