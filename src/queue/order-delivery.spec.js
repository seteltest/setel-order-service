/* eslint-disable arrow-body-style */
jest.mock('@services/order');
const { updateOrderStatus } = require('@services/order');
const { APIError } = require('@utils/APIError');
const queue = require('./order-delivery.queue');

describe('Test order delivery', () => {
  beforeEach(() => {});

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should throw API Error while creating payment', (done) => {
    updateOrderStatus.mockRejectedValue(APIError.withCode('UNSPECIFIED'));
    return queue.orderDeliveryProcess({ data: {} }).catch((err) => {
      expect(err.errors[0].errorCode).toBe('UNSPECIFIED');
      done();
    });
  });

  it('should return payment object', (done) => {
    updateOrderStatus.mockResolvedValue({ id: '1' });
    return queue.orderDeliveryProcess({ data: {} }).then((response) => {
      expect(response).toBe(true);
      done();
    });
  });
});
