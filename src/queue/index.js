const Queue = require('bee-queue');
const { queueSetting } = require('@config/vars');
const { orderDeliveryProcess } = require('./order-delivery.queue');

const orderDeliveryQueue = new Queue('orderDelivery', queueSetting);

const paymentCreationQueue = new Queue('paymentCreation', queueSetting);

orderDeliveryQueue.process(async job => orderDeliveryProcess(job));

module.exports = {
  orderDeliveryQueue,
  paymentCreationQueue
};
