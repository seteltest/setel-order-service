/**
 * Helper Utility
 *
 */
const httpStatus = require('http-status');
const { DISCOUNT_TYPE } = require('@config/vars');

/**
 * Utility function to handle response
 * @param {Object} res                        Response object of express
 * @param {String} responseMessage            The response message which needs to send
 * @param {any} response                      The response object which needs to send
 * @param {Number} statusCode                 The status code of the request
 */
const OK = (res, responseMessage = 'OK', response = {}, status = httpStatus.OK) => {
  res.status(status);
  return res.json({
    responseCode: status,
    responseMessage,
    response
  });
};

/**
 * Utility function to generate random number
 * @param {Number} min
 * @param {Number} max
 */
const randomNumberGenerator = (min, max) => Math.floor(Math.random() * (max - min) + min);

/**
 * Construct pagination response
 * @param {Array} items               Total items to be returned in response
 * @param {Integer} totalCount        Total items count available in source
 * @param {Integer} pageNumber        Page number
 * @param {Integer} pageSize          Page size
 */
const constructPagination = (items, totalCount, pageNumber, pageSize) => ({
  items,
  pagination: {
    pageNumber,
    pageSize,
    totalCount,
    itemCount: items.length
  }
});

/**
 * Utitlity function to formulate the order
 * @param {Object} body         Request Body
 * @param {Array} result        Array of promises
 */
const formulateOrder = (body, result) => {
  let amount = 0;
  const meta = [];
  const prices = result[0];
  const discount = result[1] || null;
  body.orderItems.forEach((item) => {
    const price = item.quantity * prices[item.sku];
    meta.push({ ...item, price });
    amount += price;
  });
  let amountToBePaid = amount;
  if (discount) {
    if (discount.type === DISCOUNT_TYPE.PERCENT) {
      amountToBePaid = amount - ((discount.value / amount) * 100);
    } else {
      amountToBePaid = amount - discount.value;
    }
  }
  amountToBePaid = amountToBePaid.toFixed(2);
  return {
    userId: body.userId,
    currency: body.currency,
    coupon: body.coupon,
    amount,
    discount,
    amountToBePaid,
    meta
  };
};

/**
 * Get User Type
 * @param {Object} headers        Request Headers
 * @param {Object} body           Request Body
 */
const getOrderUser = (headers, body) => {
  let user = headers.userid;
  if (headers.usertype !== 'user' && body.userId) {
    user = body.userId;
  }
  return user;
};

module.exports = {
  OK,
  randomNumberGenerator,
  constructPagination,
  formulateOrder,
  getOrderUser
};
