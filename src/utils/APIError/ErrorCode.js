module.exports = {
  UNSPECIFIED: {
    errTitle: 'Error code not specified',
    errDesc: 'Please try again, if problem still persist, please contact web master',
    errDebugDesc: 'Error code not specified in the system'
  },
  UNKNOWN: {
    errTitle: 'Oops...something went wrong',
    errDesc: 'System is not responding properly',
    errDebugDesc: 'System is not able to handle the error gracefully'
  },
  UNAUTHORIZED: {
    errTitle: 'Access Denied. Invalid Session Token',
    errDesc: 'This name already exist, please choose another name',
    errDebugDesc: 'Client with that name is already exist'
  },
  FORBIDDEN: {
    errTitle: 'Access Denied. Missing Authentication Token.',
    errDesc: 'Missing Authentication Token',
    errDebugDesc: 'Client with that name is already exist'
  },
  NOT_FOUND: {
    errTitle: 'Oops! Something is wrong',
    errDesc: 'The resource you are looking for does not exist!',
    errDebugDesc: 'Client with that name is already exist'
  },
  ERROR_DATABASE_CONNECTIVITY: {
    errTitle: 'Database Connection Error!',
    errDesc: 'Seems we are not able to connect Database',
    errDebugDesc: 'Maybe wrong database information is provided'
  },
  INVALID_COUPON: {
    errTitle: 'The coupon code you are trying to redeem is not available',
    errDesc: 'Invalid coupon code',
    errDebugDesc: 'The coupon code does not exist or not valid anymore'
  },
  ORDER_SERVICE_DOWN: {
    errTitle: 'The order service down',
    errDesc: 'Seems order service is not up',
    errDebugDesc: 'Order service down'
  },
  ORDER_NOT_UPDATED: {
    errTitle: 'The order failed to update',
    errDesc: 'The resource you are trying to update is failed',
    errDebugDesc: 'Order updation is failed'
  },
  ORDER_NOT_FOUND: {
    errTitle: 'The order you are looking for is not available',
    errDesc: 'The resource you are looking for does not exist!',
    errDebugDesc: 'Order is removed or never exist in first place'
  },
  ORDER_NOT_CREATED: {
    errTitle: 'Order failed to create',
    errDesc: 'Order is not able create',
    errDebugDesc: 'Error while creating order'
  }
};
