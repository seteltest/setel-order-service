/**
 * DbError Utility
 *
 */
const { APIError } = require('@utils/APIError');
const { logger } = require('@utils/logger');
const { Sequelize } = require('@models/');

/**
 * Function to return readable error for mysql database error
 * @param {object} err          The error object of mysql or APIError
 * @param {string} code         The recognized error code
 */
const throwReadableDBError = (err, code) => {
  if (err instanceof APIError) {
    return err;
  }
  logger.error('DB Error ', err.name);
  if (err instanceof Sequelize.ValidationError) {
    const error = err.errors[0];
    return APIError.withCode(code || 'UNSPECIFIED', undefined, { class: err.name, violation: error.type, field: error.path });
  }
  if (err instanceof Sequelize.ConnectionError) {
    return APIError.withCode(err.code || 'ERROR_DATABASE_CONNECTIVITY', undefined, { class: err.name, violation: err.sqlMessage, field: err.code });
  }
  return APIError.withCode(code || 'UNSPECIFIED', undefined, { class: err.name, violation: err.message });
};

module.exports = {
  throwReadableDBError
};
