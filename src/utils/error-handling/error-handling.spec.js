/* eslint-disable arrow-body-style */
const httpStatus = require('http-status');
const { Sequelize } = require('@models');
const util = require('./error-handling.util');
const { APIError } = require('../APIError');

describe('Utility - dbError', () => {
  beforeEach(() => { });

  afterEach(() => { });

  it('should return the API error with status code 101', () => {
    const dbError = util.throwReadableDBError(APIError.withCode('UNKNOWN', httpStatus.SERVICE_UNAVAILABLE));
    expect(dbError).toHaveProperty('name');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('status');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('isPublic');
    expect(dbError).toHaveProperty('route');
    expect(dbError).toHaveProperty('isOperational');
    expect(dbError.name).toBe('APIError');
    expect(dbError.status).toBe(503);
  });

  it('should return the sequelize error', () => {
    const dbError = util.throwReadableDBError(new Sequelize.SequelizeScopeError('Invalid Column'));
    expect(dbError).toHaveProperty('name');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('status');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('isPublic');
    expect(dbError).toHaveProperty('route');
    expect(dbError).toHaveProperty('isOperational');
  });

  it('should return the sequelize error with custom code', () => {
    const dbError = util.throwReadableDBError(new Sequelize.SequelizeScopeError('Invalid Column'), 'UNKNOWN');
    expect(dbError).toHaveProperty('name');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('status');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('isPublic');
    expect(dbError).toHaveProperty('route');
    expect(dbError).toHaveProperty('isOperational');
    expect(dbError.errors[0].errorCode).toBe('UNKNOWN');
  });

  it('should return the sequelize validation error', () => {
    const dbError = util.throwReadableDBError(new Sequelize.ValidationError('Invalid Column', [{ name: 'UniqueVoilation', class: 'UniqueConstraint', path: 'feed' }]));
    expect(dbError).toHaveProperty('name');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('status');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('isPublic');
    expect(dbError).toHaveProperty('route');
    expect(dbError).toHaveProperty('isOperational');
  });

  it('should return the sequelize validation error with custom code', () => {
    const dbError = util.throwReadableDBError(new Sequelize.ValidationError('Invalid Column', [{ name: 'UniqueVoilation', class: 'UniqueConstraint', path: 'feed' }]), 'UNKNOWN');
    expect(dbError).toHaveProperty('name');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('status');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('isPublic');
    expect(dbError).toHaveProperty('route');
    expect(dbError).toHaveProperty('isOperational');
    expect(dbError.errors[0].errorCode).toBe('UNKNOWN');
  });

  it('should return the sequelize connection error with custom code', () => {
    const dbError = util.throwReadableDBError(new Sequelize.ConnectionError('Invalid Database Configuration', [{ name: 'UniqueVoilation', class: 'UniqueConstraint', path: 'feed' }]), 'UNKNOWN');
    expect(dbError).toHaveProperty('name');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('status');
    expect(dbError).toHaveProperty('errors');
    expect(dbError).toHaveProperty('isPublic');
    expect(dbError).toHaveProperty('route');
    expect(dbError).toHaveProperty('isOperational');
    expect(dbError.errors[0].errorCode).toBe('ERROR_DATABASE_CONNECTIVITY');
  });
});
