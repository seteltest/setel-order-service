/**
 * Order Service
 *
 */
const _ = require('lodash');
const { Op } = require('sequelize');
const { ORDER_STATUS } = require('@config/vars');
const { APIError } = require('@utils/APIError');
const { throwReadableDBError } = require('@utils/error-handling');
const { Order } = require('@models');

/**
 * Function to create coupon
 * @param {Object} body            Request body
 *
 * @returns Order Object
 *
 * @public
 */
const createOrder = async (body) => {
  try {
    const params = _.pick(body, ['userId', 'currency', 'amount', 'coupon', 'discount', 'amountToBePaid', 'meta']);
    params.status = ORDER_STATUS.CREATED;
    const order = await Order.create(params);
    return order;
  } catch (err) {
    throw throwReadableDBError(err, 'ORDER_NOT_CREATED');
  }
};

/**
 * Function to get the detail of order
 * @param {Integer} orderId
 *
 * @returns Order Object
 *
 * @public
 */
const getOrder = async (orderId, userType, userId) => {
  try {
    const where = {
      id: { [Op.eq]: orderId }
    };
    if (userType === 'user') {
      where.userId = { [Op.eq]: userId };
    }
    const order = await Order.findOne({ where });
    if (!order) {
      throw APIError.withCode('ORDER_NOT_FOUND', 404);
    }
    return order;
  } catch (err) {
    throw throwReadableDBError(err, 'ORDER_SERVICE_DOWN');
  }
};

/**
 * Function to update order status
 * @param {UUID} orderId            Order ID
 * @param {String} status           Status to be updated
 *
 * @returns Order Object
 *
 * @public
 */
const updateOrderStatus = async (orderId, status) => {
  try {
    const [rowsUpdate, [updatedOrder]] = await Order.update(
      {
        status: ORDER_STATUS[status]
      },
      {
        returning: true,
        where: {
          id: { [Op.eq]: orderId },
          status: { [Op.notIn]: [ORDER_STATUS.CANCELLED, ORDER_STATUS.DELIVERED] }
        }
      }
    );
    if (!rowsUpdate) {
      throw APIError.withCode('ORDER_NOT_FOUND');
    }
    return updatedOrder;
  } catch (err) {
    throw throwReadableDBError(err, 'ORDER_NOT_UPDATED');
  }
};

/**
 * Function to list the orders
 * @param {Integer} pageNumber
 * @param {Integer} pageSize
 * @param {String} userType
 * @param {UUID} userId
 *
 * @returns Order Object List
 *
 * @public
 */
const listOrder = async (pageNumber, pageSize, userType, userId) => {
  try {
    let where = {};
    if (userType === 'user') {
      where = {
        userId: { [Op.eq]: userId }
      };
    }
    const { count, rows } = await Order.findAndCountAll({
      offset: pageNumber * pageSize,
      limit: pageSize,
      where
    });
    return { count, rows };
  } catch (err) {
    throw throwReadableDBError(err, 'ORDER_SERVICE_DOWN');
  }
};

/**
 * Function to cancel the order by user itself
 * @param {String} orderId
 * @param {String} userId
 */
const cancelOrder = async (orderId, userId) => {
  try {
    const [rowsUpdate, [updatedOrder]] = await Order.update(
      {
        status: ORDER_STATUS.CANCELLED
      },
      {
        returning: true,
        where: {
          [Op.and]: {
            id: { [Op.eq]: orderId },
            userId: { [Op.eq]: userId }
          }
        }
      }
    );
    if (!rowsUpdate) {
      throw APIError.withCode('ORDER_NOT_FOUND');
    }
    return updatedOrder;
  } catch (err) {
    throw throwReadableDBError(err, 'ORDER_NOT_UPDATED');
  }
};

module.exports = {
  createOrder,
  getOrder,
  updateOrderStatus,
  listOrder,
  cancelOrder
};
