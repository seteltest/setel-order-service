const {
  createOrder,
  getOrder,
  updateOrderStatus,
  listOrder,
  cancelOrder
} = require('./order.service');

module.exports = {
  createOrder,
  getOrder,
  updateOrderStatus,
  listOrder,
  cancelOrder
};
