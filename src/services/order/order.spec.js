/* eslint-disable arrow-body-style */
const service = require('./order.service');

jest.mock('@models');
const { Order } = require('@models');


describe('Service - order', () => {
  beforeEach(() => {});

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should failed to create order', (done) => {
    Order.create = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.createOrder({}).catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_NOT_CREATED');
      done();
    });
  });

  it('should create new order', (done) => {
    Order.create = jest.fn(() => Promise.resolve({}));
    return service.createOrder({}).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });

  it('should failed to get order', (done) => {
    Order.findOne = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.getOrder(1).catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_SERVICE_DOWN');
      done();
    });
  });

  it('should failed to get order if not exist', (done) => {
    Order.findOne = jest.fn(() => Promise.resolve(null));
    return service.getOrder(1).catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_NOT_FOUND');
      done();
    });
  });

  it('should return order detail', (done) => {
    Order.findOne = jest.fn(() => Promise.resolve({}));
    return service.getOrder(1, 'user', 1).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });

  it('should failed to update order', (done) => {
    Order.update = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.updateOrderStatus(1, 'CONFIRMED').catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_NOT_UPDATED');
      done();
    });
  });

  it('should failed to find order while updating', (done) => {
    Order.update = jest.fn(() => Promise.resolve([0, [{}]]));
    return service.updateOrderStatus(1, 'CONFIRMED').catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_NOT_FOUND');
      done();
    });
  });

  it('should update the order status', (done) => {
    Order.update = jest.fn(() => Promise.resolve([1, [{}]]));
    return service.updateOrderStatus(1, 'CONFIRMED').then((response) => {
      expect(response).toBeObject();
      done();
    });
  });

  it('should failed to list order', (done) => {
    Order.findAndCountAll = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.listOrder(0, 10, 'user', 1).catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_SERVICE_DOWN');
      done();
    });
  });

  it('should return list of order', (done) => {
    Order.findAndCountAll = jest.fn(() => Promise.resolve({ count: 0, rows: [] }));
    return service.listOrder(0, 10, 'admin', 1).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });

  it('should failed to cancel order', (done) => {
    Order.update = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.cancelOrder(1, 2).catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_NOT_UPDATED');
      done();
    });
  });

  it('should failed to find order while cancel', (done) => {
    Order.update = jest.fn(() => Promise.resolve([0, [{}]]));
    return service.cancelOrder(1, 2).catch((err) => {
      expect(err.errors[0].errorCode).toBe('ORDER_NOT_FOUND');
      done();
    });
  });

  it('should cancel the order status', (done) => {
    Order.update = jest.fn(() => Promise.resolve([1, [{}]]));
    return service.cancelOrder(1, 1).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });
});
