/**
 * Coupon Service
 *
 */
const { randomNumberGenerator } = require('@utils/helper');
const { DISCOUNT_TYPE } = require('@config/vars');
const { APIError } = require('@utils/APIError');

/**
  * This is a mock function,
  * whose function is to talk to coupon service (micro), verifies the coupon and get details related to that
  *
  * @param {String} couponCode        Coupon Code which user tries to redeem
  *
  * @return Coupon Object
  *
  * @public
  */
const getCouponValue = async (couponCode) => {
  if (couponCode !== 'FREETRIAL') {
    throw APIError.withCode('INVALID_COUPON');
  }
  // mock logic to get disount in % or amount
  /* istanbul ignore next */
  let type = DISCOUNT_TYPE.AMOUNT;
  let value = randomNumberGenerator(10, 15);
  const randomType = randomNumberGenerator(1, 10);
  if (randomType > 5) {
    type = DISCOUNT_TYPE.PERCENT;
    value = randomNumberGenerator(2, 5);
  }
  return {
    type,
    value
  };
};

module.exports = {
  getCouponValue
};
