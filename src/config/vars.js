const path = require('path');

// import .env variables
require('dotenv-safe').load({
  path: path.join(process.cwd(), '.env'),
  sample: path.join(process.cwd(), '.env.example')
});

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  serviceName: 'setel-order-service',
  http: {
    timeout: 5000,
    responseType: 'json',
    responseEncoding: 'utf8',
    retries: 3
  },
  sqsConfig: {
    accountId: process.env.SQS_ACCOUNT,
    queueName: process.env.QUEUE_NAME
  },
  ORDER_STATUS: {
    CREATED: 'CREATED',
    CONFIRMED: 'CONFIRMED',
    CANCELLED: 'CANCELLED',
    DELIVERED: 'DELIVERED',
    PAYMENT_NOT_CREATED: 'PAYMENT_NOT_CREATED'
  },
  DISCOUNT_TYPE: {
    AMOUNT: 'AMOUNT',
    PERCENT: 'PERCENT'
  },
  queueSetting: {
    redis: {
      host: process.env.CACHE_HOST,
      port: process.env.CACHE_PORT
    },
    activateDelayedJobs: true,
    removeOnSuccess: true
  },
  autoOrderDeliveryTime: 5 // in minutes
};
